[![pipeline status](https://gitlab.com/wpdesk/wp-pro-woocommerce-shipping/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-pro-woocommerce-shipping/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/wp-pro-woocommerce-shipping/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-pro-woocommerce-shipping/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/wp-pro-woocommerce-shipping/v/stable)](https://packagist.org/packages/wpdesk/wp-pro-woocommerce-shipping) 
[![Total Downloads](https://poser.pugx.org/wpdesk/wp-pro-woocommerce-shipping/downloads)](https://packagist.org/packages/wpdesk/wp-pro-woocommerce-shipping) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/wp-pro-woocommerce-shipping/v/unstable)](https://packagist.org/packages/wpdesk/wp-pro-woocommerce-shipping) 
[![License](https://poser.pugx.org/wpdesk/wp-pro-woocommerce-shipping/license)](https://packagist.org/packages/wpdesk/wp-pro-woocommerce-shipping) 

WooCommerce Shipping
====================

